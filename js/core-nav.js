$(document).ready(function () {

    /*GLOBAL GO BACK*/
    function goBack() {
        window.history.back();
    }

    $('.leftArrow').click(function () {
        goBack();
    });

    /*GLOBAL GO HOME*/
    $('.logo, .logoWhite, .logoRed').click(function () {
        location.href = "../theme.html";
    });

    /*GLOBAL SHOW MAP*/
    var mapOpen = 0; //0 = map closed

    $('#open-map').click(function () {
        location.href = '../map.html';
    });

    $('#close-map').click(function () {
        goBack();
    });

    /*NAVIGATION*/
    $('#index-right').click(function () {
        location.href = '../theme.html';
    });

    $('#nature-button').click(function () {
        location.href = '../nature.html';
    });

    $('#history-button').click(function () {
        location.href = '../heritage.html';
    });

    $('#landscape-button').click(function () {
        location.href = '../landscape.html';
    });

    $('#nature-landing-right').click(function () {
        location.href = "../finder.html";
    });

    $('#history-landing-right').click(function () {
        location.href = "../grid.html";
    });

    $('#landscape-landing-right').click(function () {
        location.href = "../360deg.html";
    });


    /*Grid to history page*/
    $('#left-button-back').click(function () {
        location.href = "../heritage.html";
    });
    
});