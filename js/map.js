$(document).ready(function () {

    //Hide the location picker by default
    $('.location-picker').hide();

    /*SHOW MAP*/
    var mapOpen = 0; //0 = map closed

    $('#open-map').click(function () {
        $('#broadsMap').slideDown();
    });

    $('#close-map').click(function () {
        $('#broadsMap').slideUp();
    });

    var menuOpen = 0;

    $('#current-location').click(function () {

        if (menuOpen === 0) {
            $('.location-picker').slideDown();
            menuOpen = 1;
        }

        else if (menuOpen === 1) {
            $('.location-picker').slideUp();
            menuOpen = 0;
        }
    });


    //URLs for the different maps
    var urls = [
        "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d9721.990926489985!2d1.6953492468776337!3d52.4701231983878!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47da1b1d3e5e5a09%3A0x50e1ca8bbc26630!2sOulton+Broad%2C+Lowestoft+NR33+9JY!5e0!3m2!1sen!2suk!4v1554147550023!5m2!1sen!2suk",
        "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2422.866616538652!2d1.7096507161081314!3d52.608181779830936!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47da06a1d094d407%3A0xdf020d01096dde50!2sBroadland-Great+Yarmouth+Rugby+Club!5e0!3m2!1sen!2suk!4v1554148311879!5m2!1sen!2suk",
        "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d19392.082932793597!2d1.6498948042351966!3d52.5875058089196!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47da0390f3efe97b%3A0x816d72790f8ce753!2sBurgh+Castle%2C+Great+Yarmouth!5e0!3m2!1sen!2suk!4v1554148350225!5m2!1sen!2suk",
        "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d9710.53075259774!2d1.6428272483841024!3d52.52198612703212!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47da03304dbe4f31%3A0xc0bbbba3bd8be28e!2sHerringfleet%2C+Lowestoft+NR32+5QS!5e0!3m2!1sen!2suk!4v1554148397228!5m2!1sen!2suk",
        "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2430.604820507463!2d1.6909699161038274!3d52.4681839798044!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47da1b9aeeb8f487%3A0x4b04bcc70c90115f!2sCarlton+Marshes+Nature+Reserve!5e0!3m2!1sen!2suk!4v1554148443402!5m2!1sen!2suk",
        "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2430.8307937007103!2d1.5634023161037234!3d52.46409177980365!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47d9f63b4afc9441%3A0x41cf0eb9d3940d48!2sFen+Ln%2C+Beccles!5e0!3m2!1sen!2suk!4v1554148491347!5m2!1sen!2suk",
        "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2430.8554709761997!2d1.514997516103693!3d52.46364487980361!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47d9f6ecdc9ce26d%3A0xd07e3bb7ee3c8b8!2sLocks+Inn+Geldeston!5e0!3m2!1sen!2suk!4v1554148547044!5m2!1sen!2suk",
        "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d9726.555740029213!2d1.6177732483486245!3d52.44945512681335!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47da1c34f186d3ed%3A0xbf9c25ca5322ebf!2sNorth+Cove%2C+Beccles+NR34+7PP!5e0!3m2!1sen!2suk!4v1554148587729!5m2!1sen!2suk",
        "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d9705.651521260277!2d1.633123248394905!3d52.544056127099054!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47da031b0d55d175%3A0x85c9dedeeda48f3d!2sFritton%2C+Great+Yarmouth+NR31+9EZ!5e0!3m2!1sen!2suk!4v1554148668721!5m2!1sen!2suk",
        "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d9705.650504143032!2d1.650258698394926!3d52.54406072709909!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47da036ad9a7bcef%3A0x47da033d206e7809!2sFritton+Decoy!5e0!3m2!1sen!2suk!4v1554148697934!5m2!1sen!2suk"
    ];

    var cafeurls = [
        "https://maps.google.com/maps?q=cafes%20in%20oulton%20broad&t=&z=15&ie=UTF8&iwloc=&output=embed",
        "https://maps.google.com/maps?q=cafes%20in%20breydon%20water&t=&z=13&ie=UTF8&iwloc=&output=embed",
        "https://maps.google.com/maps?q=cafes%20in%20burgh%20castle&t=&z=9&ie=UTF8&iwloc=&output=embed",
        "https://maps.google.com/maps?q=cafes%20in%20herringfleet&t=&z=11&ie=UTF8&iwloc=&output=embed",
        "https://maps.google.com/maps?q=cafes%20in%20carlton%20colville&t=&z=13&ie=UTF8&iwloc=&output=embed",
        "https://maps.google.com/maps?q=cafes%20in%20beccles&t=&z=9&ie=UTF8&iwloc=&output=embed",
        "https://maps.google.com/maps?q=cafes%20in%20geldeston&t=&z=13&ie=UTF8&iwloc=&output=embed",
        "https://maps.google.com/maps?q=cafes%20in%20north%20cove&t=&z=13&ie=UTF8&iwloc=&output=embed",
        "https://maps.google.com/maps?q=cafes%20in%20fritton&t=&z=11&ie=UTF8&iwloc=&output=embed",
        "https://maps.google.com/maps?q=cafes%20in%20fritton&t=&z=11&ie=UTF8&iwloc=&output=embed"
    ];

    var toileturls = [
        "https://maps.google.com/maps?q=toilets%20in%20lowestoft&t=&z=13&ie=UTF8&iwloc=&output=embed",
        "https://maps.google.com/maps?q=toilets%20in%20breydon%20water&t=&z=11&ie=UTF8&iwloc=&output=embed",
        "https://maps.google.com/maps?q=toilets%20in%20burgh%20castle&t=&z=11&ie=UTF8&iwloc=&output=embed",
        "https://maps.google.com/maps?q=toilets%20in%20herringfleet&t=&z=11&ie=UTF8&iwloc=&output=embed",
        "https://maps.google.com/maps?q=toilets%20in%20lowestoft&t=&z=13&ie=UTF8&iwloc=&output=embed",
        "https://maps.google.com/maps?q=toilets%20in%20beccles&t=&z=15&ie=UTF8&iwloc=&output=embed",
        "https://maps.google.com/maps?q=toilets%20in%20beccles&t=&z=13&ie=UTF8&iwloc=&output=embed",
        "https://maps.google.com/maps?q=toilets%20in%20north%20cove&t=&z=13&ie=UTF8&iwloc=&output=embed",
        "https://maps.google.com/maps?q=toilets%20in%20fritton&t=&z=11&ie=UTF8&iwloc=&output=embed",
        "https://maps.google.com/maps?q=toilets%20in%20fritton&t=&z=11&ie=UTF8&iwloc=&output=embed"
    ];

    var stationurls = [
        "https://maps.google.com/maps?q=public%20transport%20in%20oulton%20broad&t=&z=15&ie=UTF8&iwloc=&output=embed",
        "https://maps.google.com/maps?q=public%20transport%20in%20breydon%20water&t=&z=11&ie=UTF8&iwloc=&output=embed",
        "https://maps.google.com/maps?q=public%20transport%20in%20burgh%20castle&t=&z=13&ie=UTF8&iwloc=&output=embed",
        "https://maps.google.com/maps?q=public%20transport%20in%20herringfleet&t=&z=11&ie=UTF8&iwloc=&output=embed",
        "https://maps.google.com/maps?q=public%20transport%20in%20carlton%20colville&t=&z=13&ie=UTF8&iwloc=&output=embed",
        "https://maps.google.com/maps?q=public%20transport%20in%20beccles&t=&z=9&ie=UTF8&iwloc=&output=embed",
        "https://maps.google.com/maps?q=public%20transport%20in%20geldeston&t=&z=13&ie=UTF8&iwloc=&output=embed",
        "https://maps.google.com/maps?q=public%20transport%20in%20north%20cove&t=&z=13&ie=UTF8&iwloc=&output=embed",
        "https://maps.google.com/maps?q=public%20transport%20in%20fritton%2C%20norfolk&t=&z=13&ie=UTF8&iwloc=&output=embed",
        "https://maps.google.com/maps?q=public%20transport%20in%20fritton%2C%20norfolk&t=&z=13&ie=UTF8&iwloc=&output=embed"
    ];

    var parkingurls = [
        "https://maps.google.com/maps?q=parking%20in%20oulton%20broad&t=&z=15&ie=UTF8&iwloc=&output=embed",
        "https://maps.google.com/maps?q=parking%20in%20great%20yarmouth&t=&z=13&ie=UTF8&iwloc=&output=embed",
        "https://maps.google.com/maps?q=parking%20in%20burgh%20castle&t=&z=15&ie=UTF8&iwloc=&output=embed",
        "https://maps.google.com/maps?q=parking%20in%20herringfleet&t=&z=15&ie=UTF8&iwloc=&output=embed",
        "https://maps.google.com/maps?q=parking%20in%20carlton%20colville&t=&z=13&ie=UTF8&iwloc=&output=embed",
        "https://maps.google.com/maps?q=parking%20in%20beccles&t=&z=15&ie=UTF8&iwloc=&output=embed",
        "https://maps.google.com/maps?q=parking%20in%20geldeston&t=&z=13&ie=UTF8&iwloc=&output=embed",
        "https://maps.google.com/maps?q=parking%20in%20north%20cove&t=&z=9&ie=UTF8&iwloc=&output=embed",
        "https://maps.google.com/maps?q=parking%20in%20fritton&t=&z=11&ie=UTF8&iwloc=&output=embed",
        "https://maps.google.com/maps?q=parking%20in%20fritton&t=&z=11&ie=UTF8&iwloc=&output=embed"
    ];

    var locations = ["Oulton Broad", "Breydon Water", "Burgh Castle", "Herringfleet", "Peto's Marsh", "Beccles", "Geldeston", "North Cove", "Caldecott Mill", "Fritton Decoy"];

    var arrayIndex = 0; //default is index 0 - Oulton Broad

    //----------FORWARDS/BACKWARDS BUTTONS---------//
    function nextLocation() {
        closeMenu();
        arrayIndex++;
        checkArray();
        updateMap();
        updateLocation();
    }

    function previousLocation() {
        closeMenu();
        arrayIndex--;
        checkArray();
        updateMap();
        updateLocation();
    }

    function checkArray() {
        if (arrayIndex > 9) {
            arrayIndex = 0; //reset the arrayIndex to the 0th pointer if the index is greater than 9 - back to the beginning
        }

        if (arrayIndex < 0) {
            arrayIndex = 9; //reset the arrayIndex to the 9th pointer if the index is less than 0 - back to the end
        }
    }

    //Update user's location (if they press forward/backward button)
    function updateLocation() {
        document.getElementById("current-location").innerHTML = locations[arrayIndex];
    }

    //Update map SRC for iFrame (call within 'nextLocation' or 'previousLocation')
    function updateMap() {
        $('#map').fadeOut(1000, function () {
            $('#map').attr('src', urls[arrayIndex]); //load URL from the array
            $('#map').fadeIn(1000);
        });
    }

    //Forward location button
    $('#forwards').click(function () {
        nextLocation();
    });

    //Backward location button
    $('#backwards').click(function () {
        previousLocation();
    });


    //----------MENU----------//
    //Change map on menu item click
    $('#oulton-broad').click(function () {
        $('#map').fadeOut(1000, function () {
            $('#map').attr('src', urls[0]);
            $('#map').fadeIn(1000);
        });
        document.getElementById("current-location").innerHTML = locations[0];
        arrayIndex = 0;
        closeMenu();
    });

    $('#breydon-water').click(function () {
        $('#map').fadeOut(1000, function () {
            $('#map').attr('src', urls[1]);
            $('#map').fadeIn(1000);
        });
        document.getElementById("current-location").innerHTML = locations[1];
        arrayIndex = 1;
        closeMenu();
    });

    $('#burgh-castle').click(function () {
        $('#map').fadeOut(1000, function () {
            $('#map').attr('src', urls[2]);
            $('#map').fadeIn(1000);
        });
        document.getElementById("current-location").innerHTML = locations[2];
        arrayIndex = 2;
        closeMenu();
    });

    $('#herringfleet').click(function () {
        $('#map').fadeOut(1000, function () {
            $('#map').attr('src', urls[3]);
            $('#map').fadeIn(1000);
        });
        document.getElementById("current-location").innerHTML = locations[3];
        arrayIndex = 3;
        closeMenu();
    });

    $('#petos-marsh').click(function () {
        $('#map').fadeOut(1000, function () {
            $('#map').attr('src', urls[4]);
            $('#map').fadeIn(1000);
        });
        document.getElementById("current-location").innerHTML = locations[4];
        arrayIndex = 4;
        closeMenu();
    });

    $('#beccles').click(function () {
        $('#map').fadeOut(1000, function () {
            $('#map').attr('src', urls[5]);
            $('#map').fadeIn(1000);
        });
        document.getElementById("current-location").innerHTML = locations[5];
        arrayIndex = 5;
        closeMenu();
    });

    $('#geldeston').click(function () {
        $('#map').fadeOut(1000, function () {
            $('#map').attr('src', urls[6]);
            $('#map').fadeIn(1000);
        });
        document.getElementById("current-location").innerHTML = locations[6];
        arrayIndex = 6;
        closeMenu();
    });

    $('#north-cove').click(function () {
        $('#map').fadeOut(1000, function () {
            $('#map').attr('src', urls[7]);
            $('#map').fadeIn(1000);
        });
        document.getElementById("current-location").innerHTML = locations[7];
        arrayIndex = 7;
        closeMenu();
    });

    $('#caldecott-mill').click(function () {
        $('#map').fadeOut(1000, function () {
            $('#map').attr('src', urls[8]);
            $('#map').fadeIn(1000);
        });
        document.getElementById("current-location").innerHTML = locations[8];
        arrayIndex = 8;
        closeMenu();
    });

    $('#fritton-decoy').click(function () {
        $('#map').fadeOut(1000, function () {
            $('#map').attr('src', urls[9]);
            $('#map').fadeIn(1000);
        });
        document.getElementById("current-location").innerHTML = locations[9];
        arrayIndex = 9;
        closeMenu();
    });

    //Close menu after item clicked
    function closeMenu() {
        $('.location-picker').slideUp();
        menuOpen = 0;
    }


    //----------FACILITIES BUTTONS----------//
    //Cafes
    $('#cafes-button').click(function () {
        $('#map').fadeOut(1000, function () {
            $('#map').attr('src', cafeurls[arrayIndex]); //the arrayIndex value contains a number between 0 and 9 which is linked to the location (it is always the same as the current location)
            $('#map').fadeIn(1000);
        });
        closeMenu();
    });


    //Toilets
    $('#toilets-button').click(function () {
        $('#map').fadeOut(1000, function () {
            $('#map').attr('src', toileturls[arrayIndex]);
            $('#map').fadeIn(1000);
        });
        closeMenu();
    });

    //Stations
    $('#stations-button').click(function () {
        $('#map').fadeOut(1000, function () {
            $('#map').attr('src', stationurls[arrayIndex]);
            $('#map').fadeIn(1000);
        });
        closeMenu();
    });

    //Parking
    $('#parking-button').click(function () {
        $('#map').fadeOut(1000, function () {
            $('#map').attr('src', parkingurls[arrayIndex]);
            $('#map').fadeIn(1000);
        });
        closeMenu();
    });


    //---------OTHER----------//
    //Close map
    $('#close-map').click(function () {
        $('.container').slideDown();
    });



});
