$(document).ready(function () {

    //stage1-2
    $('#spring-button').click(function () {
        location.href = "stage2/spring.html";
    });

    $('#summer-button').click(function () {
        location.href = "stage2/summer.html";
    });

    $('#autumn-button').click(function () {
        location.href = "stage2/autumn.html";
    });

    $('#winter-button').click(function () {
        location.href = "stage2/winter.html";
    });

    //stage2-3 
    $('#spring-plants-button').click(function () {
        location.href = "../stage3/spring-marginal-species.html";
    });
    
    $('#spring-animals-button').click(function () {
        location.href = "../stage3/spring-birds.html";
    });

    $('#summer-plants-button').click(function () {
        location.href = "../stage3/summer-marginal-aquatic-species.html";
    });

    $('#summer-animals-button').click(function () {
        location.href = "../stage3/summer-birds-mammals-reptiles.html";
    });

    $('#autumn-animals-button').click(function () {
        location.href = "../stage3/autumn-birds-mammals.html";
    });

    $('#autumn-plants-button').click(function () {
        location.href = "../stage3/autumn-marginal-aquatic.html";
    });

    $('#winter-animals-button').click(function () {
        location.href = "../stage3/winter-birds.html";
    });

    $('#winter-plants-button').click(function () {
        location.href = "../stage3/winter-marginal-species.html";
    });

    //stage3-4
    $('#spring-animals-birds-button').click(function () {
        location.href = "../stage4/spring-animals-birds.html";
    });

    $('#spring-plants-marginal-species-button').click(function () {
        location.href = "../stage4/spring-plants-marginalspecies.html";
    });

    $('#summer-animals-birds-button').click(function () {
        location.href = "../stage4/summer-animals-birds.html";
    });

    $('#summer-animals-mammals-button').click(function () {
        location.href = "../stage4/summer-animals-mammals.html";
    });

    $('#summer-animals-reptiles-button').click(function () {
        location.href = "../stage4/summer-animals-reptiles.html";
    });

    $('#summer-plants-aquatic-species-button').click(function () {
        location.href = "../stage4/summer-plants-aquaticspecies.html";
    });

    $('#summer-plants-marginal-species-button').click(function () {
        location.href = "../stage4/summer-plants-marginalspecies.html";
    });

    $('#autumn-animals-birds-button').click(function () {
        location.href = "../stage4/autumn-animals-birds.html";
    });

    $('#autumn-animals-mammals-button').click(function () {
        location.href = "../stage4/autumn-animals-mammals.html";
    });

    $('#autumn-aquatic-species-button').click(function () {
        location.href = "../stage4/autumn-plants-aquaticspecies.html";
    });

    $('#autumn-marginal-species-button').click(function () {
        location.href = "../stage4/autumn-plants-marginalspecies.html";
    });

    $('#winter-animals-birds-button').click(function () {
        location.href = "../stage4/winter-animals-birds.html";
    });

    $('#winter-marginal-species-button').click(function () {
        location.href = "../stage4/winter-plants-marginalspecies.html";
    });


    //stage4-5
    $('.reed-warbler').click(function () {
        location.href = "../stage5/reed-warbler.html";
    });

    $('.sedge-warbler').click(function () {
        location.href = "../stage5/sedge-warbler.html";
    });

    $('.grasshopper-warbler').click(function () {
        location.href = "../stage5/grasshopper-warbler.html";
    });

    $('.cettis-warbler').click(function () {
        location.href = "../stage5/cettis-warbler.html";
    });

    $('.common-reed').click(function () {
        location.href = "../stage5/common-reed.html";
    });

    $('.willow').click(function () {
        location.href = "../stage5/willow.html";
    });

    $('.greater-tussock-sedge').click(function () {
        location.href = "../stage5/greater-tussock-sedge.html";
    });

    $('.greater-pond-sedge').click(function () {
        location.href = "../stage5/greater-pond-sedge.html";
    });

    $('.yellow-iris').click(function () {
        location.href = "../stage5/yellow-iris.html";
    });

    $('.hemp-agrimony').click(function () {
        location.href = "../stage5/hemp-agrimony.html";
    });

    $('.water-vole').click(function () {
        location.href = "../stage5/water-vole.html";
    });

    $('.grass-snake').click(function () {
        location.href = "../stage5/grass-snake.html";
    });

    $('.spiked-water-milfoil').click(function () {
        location.href = "../stage5/spiked-water-milfoil.html";
    });

    $('.yellow-water-lily').click(function () {
        location.href = "../stage5/yellow-water-lily.html";
    });

    $('.rigid-hornwort').click(function () {
        location.href = "../stage5/rigid-hornwort.html";
    });

    $('.canadian-waterweed').click(function () {
        location.href = "../stage5/canadian-waterweed.html";
    });

    $('.marsh-harrier').click(function () {
        location.href = "../stage5/marsh-harrier.html";
    });

    $('.coot').click(function () {
        location.href = "../stage5/coot.html";
    });

    $('.grey-heron').click(function () {
        location.href = "../stage5/heron.html";
    });

    $('.moorhen').click(function () {
        location.href = "../stage5/moorhen.html";
    });

    $('.mallard').click(function () {
        location.href = "../stage5/mallard.html";
    });

    $('.black-headed-gull').click(function () {
        location.href = "../stage5/black-headed-gull.html";
    });

    $('.herring-gull').click(function () {
        location.href = "../stage5/herring-gull.html";
    });

});
